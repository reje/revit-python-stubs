# encoding: utf-8
# module Revit.References calls itself References
# from RevitNodes, Version=1.2.1.3083, Culture=neutral, PublicKeyToken=null
# by generator 1.145
# no doc
# no imports

# no functions
# classes

class RayBounce(object):
    # no doc
    @staticmethod
    def ByOriginDirection(origin, direction, maxBounces, view):
        """ ByOriginDirection(origin: Point, direction: Vector, maxBounces: int, view: View3D) -> Dictionary[str, object] """
        pass

    __all__ = [
        'ByOriginDirection',
    ]



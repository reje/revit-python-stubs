# encoding: utf-8
# module Revit.GeometryObjects calls itself GeometryObjects
# from RevitNodes, Version=1.2.1.3083, Culture=neutral, PublicKeyToken=null
# by generator 1.145
# no doc
# no imports

# no functions
# classes

class GeometryObjectSelector(object):
    # no doc
    @staticmethod
    def ByReferenceStableRepresentation(referenceString):
        """ ByReferenceStableRepresentation(referenceString: str) -> object """
        pass

    __all__ = [
        'ByReferenceStableRepresentation',
    ]



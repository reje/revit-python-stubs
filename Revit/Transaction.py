# encoding: utf-8
# module Revit.Transaction calls itself Transaction
# from RevitNodes, Version=1.2.1.3083, Culture=neutral, PublicKeyToken=null
# by generator 1.145
# no doc
# no imports

# no functions
# classes

class Transaction(object):
    # no doc
    @staticmethod
    def End(input):
        """ End(input: object) -> object """
        pass

    @staticmethod
    def Start(input):
        """ Start(input: object) -> object """
        pass



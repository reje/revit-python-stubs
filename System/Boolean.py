# encoding: utf-8
# module System.Boolean calls itself bool
# from (built-in)
# by generator 1.145
# no doc
# no imports

# functions

def __and__(x, y): # real signature unknown; restored from __doc__
    """
    __and__(x: bool, y: int) -> int
    __and__(x: int, y: bool) -> int
    __and__(x: bool, y: bool) -> bool
    """
    return 0

def __format__(self, formatSpec): # real signature unknown; restored from __doc__
    """ __format__(self: bool, formatSpec: str) -> str """
    return ""

def __int__(x): # real signature unknown; restored from __doc__
    """ __int__(x: bool) -> int """
    return 0

@staticmethod # known case of __new__
def __new__(cls, o): # real signature unknown; restored from __doc__
    """
    __new__(cls: object, o: object) -> bool
    __new__(cls: object) -> object
    """
    return False

def __or__(x, y): # real signature unknown; restored from __doc__
    """
    __or__(x: bool, y: int) -> int
    __or__(x: int, y: bool) -> int
    __or__(x: bool, y: bool) -> bool
    """
    return 0

def __rand__(x, y): # real signature unknown; restored from __doc__
    """
    __rand__(x: bool, y: int) -> int
    __rand__(x: int, y: bool) -> int
    __rand__(x: bool, y: bool) -> bool
    """
    return 0

def __repr__(self): # real signature unknown; restored from __doc__
    """ __repr__(self: bool) -> str """
    return ""

def __ror__(x, y): # real signature unknown; restored from __doc__
    """
    __ror__(x: bool, y: int) -> int
    __ror__(x: int, y: bool) -> int
    __ror__(x: bool, y: bool) -> bool
    """
    return 0

def __rxor__(x, y): # real signature unknown; restored from __doc__
    """
    __rxor__(x: bool, y: int) -> int
    __rxor__(x: int, y: bool) -> int
    __rxor__(x: bool, y: bool) -> bool
    """
    return 0

def __str__(): # real signature unknown; restored from __doc__
    """ x.__str__() <==> str(x) """
    pass

def __xor__(x, y): # real signature unknown; restored from __doc__
    """
    __xor__(x: bool, y: int) -> int
    __xor__(x: int, y: bool) -> int
    __xor__(x: bool, y: bool) -> bool
    """
    return 0

# no classes

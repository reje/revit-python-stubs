#+OPTIONS: toc:nil

* Revit Python Stubs

Generated Python stubs for Revit and Dynamo libraries.

Useful for autocompetion and code analysis in 3rd party editors such as [[https://www.jetbrains.com/pycharm/][PyCharm]], [[https://www.gnu.org/software/emacs/][Emacs]], [[http://www.vim.org/][Vim]] etc.

** Screenshot

[[./screenshot.png]]

Emacs autocompletion using Revit Python Stubs.

** Libraries

*** Revit (DB and UI)

[[./Autodesk/Revit/][Autodesk.Revit]]

Main Revit API.

*** Dynamo

**** Designscript

[[./Autodesk/DesignScript/Geometry/][Autodesk.DesignScript.Geometry]]

ProtoGeometry is a wrapper for the designscript control of the [[https://en.wikipedia.org/wiki/ShapeManager][AMS]] geometry kernel Autodesk
provides for Dynamo Geometry creation including the TSpline namespace.

**** Revit

[[./Revit/][Revit]]

Access to the Dynamo wrapped Revit API objects.

**** RevitServices

[[./RevitServices/][RevitServices]]

Access to the Document Manager which contains the Revit document instance.

*** System

[[./System][System]]

Useful .NET System libraries.

** Other resources

[[http://eirannejad.github.io/pyRevit/][pyRevit]] - IronPython scripts for Autodesk Revit

[[http://revitpythonwrapper.readthedocs.io/en/latest/][revitpythonwrapper]] - Revit Python Wrapper was created to help Python programmers write Revit API code

[[https://github.com/architecture-building-systems/revitpythonshell][revitpythonshell]] - An IronPython scripting environment for Autodesk Revit

[[http://www.revitapidocs.com/][revitapidocs]] - Online Revit API Documentation

[[https://github.com/JetBrains/intellij-community/blob/master/python/helpers/generator3.py][generator3.py]] - PyCharm's stub generator used to generate these stubs
